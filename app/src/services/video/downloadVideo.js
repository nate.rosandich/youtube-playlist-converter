export const downloadVideo = async ({ url = null } = {}) => {
    try {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ url })
        };

        const destinationUrl = `${import.meta.env.VITE_SERVER_URL}/stream`;
        const response = await fetch(`${destinationUrl}/?url=${url}`);

        if (!response.ok) {
            throw new Error('Error sending parameter to URL');
        }
        const responseData = await response.json();

        return responseData;

    } catch (err) {
        const error = `downloadVideo error: ${err.message}`;
        return Promise.reject(error);
    }
}