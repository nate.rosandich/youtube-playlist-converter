import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { VitePWA } from 'vite-plugin-pwa';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      registerType: "autoUpdate",
      srcDir: "src",
      filename: "sw.js",
      includeAssets: ['icons/favicon.svg', 'icons/favicon.ico', 'robots.txt', 'icons/apple-touch-icon.png'],
      strategies: "injectManifest",
      manifest: {
        name: 'YouTube MP3 Converter',
        short_name: 'YouTube MP3 Converter',
        description: 'Converting and downloading YouTube videos to MP3',
        start_url: "/",
        display: "standalone",
        theme_color: '#111827',
        icons: [
          {
            src: 'icons/pwa-192x192.png',
            sizes: '192x192',
            type: 'image/png',
          },
          {
            src: 'icons/pwa-512x512.png',
            sizes: '512x512',
            type: 'image/png',
          },
          {
            src: 'icons/pwa-512x512.png',
            sizes: '512x512',
            type: 'image/png',
            purpose: 'any maskable',
          }
        ]
      }
    })
  ],
  base: './',
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  }
});