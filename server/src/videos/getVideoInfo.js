import ytdl from 'ytdl-core';

export const getVideoInfo = async ({ url = null } = {}) => {

  try {
    const info = await ytdl.getBasicInfo(url, { format: 'mp4' });
    const title = info.player_response.videoDetails.title.replace(/[^\x00-\x7F]/g, "");

    return {
      title
    };
  } catch (e) {
    throw new Error("Invalid playlist URL");
  }
}