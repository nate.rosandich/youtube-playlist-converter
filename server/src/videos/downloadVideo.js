import path from 'path';
import ytdl from 'ytdl-core';
import ffmpegPath from '@ffmpeg-installer/ffmpeg';
import ffmpeg from 'fluent-ffmpeg';
import { toUrlSafeString } from '../utils/index.js'

ffmpeg.setFfmpegPath(ffmpegPath.path);

export const downloadVideo = async ({ url = null, name = null, location = null } = {}) => {

    try {
        const videoName = toUrlSafeString({ string: name });
        const videoPath = path.resolve(path.resolve(), location, `${videoName}.mp3`);

        return new Promise((resolve, reject) => {
            const stream = ytdl(url, {
                quality: 'highestaudio',
            });

            const conversion = ffmpeg(stream)
                .audioBitrate(128)
                .save(videoPath);

            conversion.on('end', resolve(videoPath));
            conversion.on('error', function (err) {
                reject(err.message);
            });
        });

    } catch (e) {
        throw new Error(e);
    }
}
