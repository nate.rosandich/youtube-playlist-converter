import ytdl from 'ytdl-core';

export const streamVideo = async ({ url = null, title = null, response = null } = {}) => {

  try {
    response.header('Content-Disposition', `attachment; filename="${title}.mp3"`);
    ytdl(url, {
      format: 'mp3',
      filter: 'audioonly',
    }).pipe(response);

  } catch (e) {
    throw new Error(e);
  }
}