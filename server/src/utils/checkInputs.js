export const checkInputs = ({ params = {}, inputs = {} } = {}) => {

    if (!params || typeof params === 'undefined' || params === "") {
        throw new Error("No params provided");
    }

    for (const propName of inputs) {
        // Check if the property exists in the object
        if (!params.hasOwnProperty(propName)) {
            throw new Error(`No ${propName} provided`);
        }
    }
    return true;
};
