export const toUrlSafeString = ({ string = null } = {}) => {
    return string
        .toLowerCase()
        .replace(/[^a-z0-9]+/g, "-")
        .replace(/(^-|-$)+/g, "");
};
