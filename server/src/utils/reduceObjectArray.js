export const reduceObjectArray = ({ originalArray = [], reducedKeys = [] } = {}) => {
    if (!originalArray || originalArray.length === 0) {
        throw new Error("Original array is empty or null");
    }

    if (!reducedKeys || reducedKeys.length === 0) {
        throw new Error("Reduced keys array is empty or null");
    }

    const newArray = originalArray.map(originalObject => {
        const newObject = {};
        reducedKeys.forEach(reducedKey => {
            if (originalObject.hasOwnProperty(reducedKey)) {
                newObject[reducedKey] = originalObject[reducedKey];
            }
        });
        return newObject;
    });
    return newArray;
}