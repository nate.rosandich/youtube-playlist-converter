import express from 'express';
import cors from 'cors'
import bodyParser from 'body-parser';

import { checkInputs } from './utils/index.js';
import { getVideoInfo, downloadVideo, streamVideo } from './videos/index.js';
import { createLocalFolder } from './storage/index.js';

const app = express();
const PORT = process.env.PORT || 3001;

app.use(cors());
app.use(bodyParser.json());

app.get("/stream", async (request, response, next) => {

  try {
    const params = request?.query;
    checkInputs({ params, inputs: ['url'] });

    const url = decodeURI(params.url);

    const { title } = await getVideoInfo({ url });
    await streamVideo({ url, title, response })

  } catch (error) {
    console.log({ error })
    next(error);
  }
});

app.get("/download", async (request, response, next) => {

  try {
    const params = request?.query;
    checkInputs({ params, inputs: ['url'] });

    const url = decodeURI(params.url);

    const title = await getVideoInfo({ url });

    const location = await createLocalFolder({ name: title });

    const videoPath = await downloadVideo({ url, title, location });

    response.json({
      url, title, location
    });

  } catch (error) {
    console.log({ error })
    next(error);
  }
});

app.use((error, request, response, next) => {
  response.status(500).send(`Error: ${error}`);
})

app.listen(PORT, () => console.log(`App listening at port ${PORT}`));
