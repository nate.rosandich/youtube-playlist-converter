import fs from 'fs';
import { toUrlSafeString } from '../utils/index.js'

export const createLocalFolder = async ({ name = null } = {}) => {

    try {
        const folderName = toUrlSafeString({ string: name });

        if (!fs.existsSync(`download`)) {
            fs.mkdirSync(`download`, (err) => {
                if (err) {
                    throw new Error("Couldn't create folder download");
                }
            });
        }

        if (!fs.existsSync(`download/${folderName}`)) {
            fs.mkdirSync(`download/${folderName}`, (err) => {
                if (err) {
                    throw new Error(`Couldn't create folder download/${folderName}`);
                }
            });
        }
        return `download/${folderName}`;

    } catch (e) {
        throw new Error(e);
    }
}
